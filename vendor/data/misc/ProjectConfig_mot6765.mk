# Only item in $(MTK_TARGET_PROJECT_FOLDER)/ProjectConfig.mk con be overlay here
include device/moto/mot6765/mot_debug_logging.mk

# Enable Trustonic T-Base solution on MT6765.
MTK_TEE_SUPPORT = yes
MTK_TEE_GP_SUPPORT = yes
TRUSTONIC_TEE_SUPPORT = yes

# Enable RCS
MTK_RCS_UA_SUPPORT = yes
MTK_UCE_SUPPORT = yes

# Enable keymaster 4.1 on mt6765 platform.
KEYMASTER_VERSION := 4.1
# disable MTK telephony log control
MTK_TELEPHONY_CONN_LOG_CTRL_SUPPORT = no

# MME disable needless features
MTK_AUDIO_ADPCM_SUPPORT = no
MTK_AUDIO_ALAC_SUPPORT = no
MTK_AUDIO_APE_SUPPORT = no
MTK_FLV_PLAYBACK_SUPPORT = no
MTK_WMV_PLAYBACK_SUPPORT = no
MTK_SLOW_MOTION_VIDEO_SUPPORT = no
MTK_HIGH_RESOLUTION_AUDIO_SUPPORT = no
# disable mtk OMA Drm v1 & CTA5 Drm
MTK_DRM_APP = no
MTK_OMADRM_SUPPORT = no
MTK_OMA_DOWNLOAD_SUPPORT = no
MTK_CTA_SET = no
